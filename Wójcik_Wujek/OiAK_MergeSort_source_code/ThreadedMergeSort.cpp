﻿#include "ThreadedMergeSort.h"
#include "SingleThreadMergeSort.h"
#include <cmath>
#include <thread>
#include <algorithm>
#include <omp.h>
#include <iostream>

#define ThreadNumberLimit 8

//sortowanie całe, scala całe raz połowa sizeof tab
void ThreadedMergeSort::mergingThread(int tab_start[], int tab_end[], int sizeOfTab)
{
	int* tmp_tab = new int[2 * sizeOfTab];
	int start_count = 0, end_count = 0;

	for (int i = 0; i < 2 * sizeOfTab; i++)
	{
		if ((tab_start[start_count] <= tab_end[end_count] && start_count < sizeOfTab) || end_count == sizeOfTab)
		{
			tmp_tab[i] = tab_start[start_count];
			start_count++;
		}
		else
		{
			if (end_count < sizeOfTab)
			{
				tmp_tab[i] = tab_end[end_count];
				end_count++;
			}
		}
	}

	for (int i = 0; i < 2 * sizeOfTab; i++)
	{
		tab_start[i] = tmp_tab[i];
	}

	delete[] tmp_tab;
}

void ThreadedMergeSort::NoThreadLimit(int tab[], int size, int step)
{
	//step = 1 on start
	int t_count = ceil(size / (2 * step));
	if (t_count == 0)
	{
		return;
	}
	std::thread* t = new std::thread[t_count];

	for (int i = 0; i < t_count; ++i)
	{
		int start = 2 * step * i;
		int end = 2 * step * i + step;

		//				int end = 2 *  step * i + (step-1)*2+1; - tu jest koniec tablicy, a nie początek

		t[i] = std::thread(mergingThread, &tab[start], &tab[end], step);
	}

	for (int i = 0; i < t_count; ++i)
	{
		t[i].join();
	}

	delete[] t;

	if (2 ^ step < size)
	{
		NoThreadLimit(tab, size, step * 2);
	}
}

void ThreadedMergeSort::ThreadLimit(int tab[], int size, int step)
{
	//step = 1 on start
	int t_count = ceil(size / (2 * step));
	if (t_count == 0)
	{
		return;
	}
	std::thread* t = new std::thread[ThreadNumberLimit];

	int done=0;

	do
	{
		int lunchedThreads = 0;
		for (int i = 0; i < ThreadNumberLimit; ++i)
		{

			int start = 2 * step * done;
			int end = 2 * step * done + step;

			if(done<t_count)
			{
				t[i] = std::thread(mergingThread, &tab[start], &tab[end], step);
				lunchedThreads++;
				done++;
			}
		}

		for (int i = 0; i < lunchedThreads; ++i)
		{
			t[i].join();
		}

	} while (done < t_count);

	

	if (2 ^ step < size)
	{
		NoThreadLimit(tab, size, step * 2);
	}

	delete[] t;
}

void ThreadedMergeSort::DivThread(int *tab, int size, int number)
{
	std::thread* t = new std::thread[number];
	for (int i = 0; i < number; i++)
	{
		t[i] = std::thread(SingleThreadMergeSort::SingleThreadBasedOnMulti, &tab[i * size / number], size / number, 1);
	}

	for (int i = 0; i < number; i++)
	{
		t[i].join();
	}

	SingleThreadMergeSort::SingleThreadBasedOnMulti(&tab[0], size, size / number);
	//mergingThread(&tab[0], &tab[size/2], size/2);	//dla jednego podziału
	delete[] t;
}


void ThreadedMergeSort::DivThreadV2(int *tab,int **divTab, int size, int number)
{

	std::thread* t = new std::thread[number];

	for (int i = 0; i < number; i++)
	{
		t[i] = std::thread(SingleThreadMergeSort::SingleThreadBasedOnMulti, divTab[i], size / number, 1);
	}
	for (int i = 0; i < number; i++)
	{
		t[i].join();
	}

	mergingOutput(divTab[0], divTab[1], size / number,tab);
	//mergingThread(&tab[0], &tab[size/2], size/2);	//dla jednego podziału
	delete[] t;
}

//wersja mergowania z podaniem wyjśiowej posortowanej tablicy
void ThreadedMergeSort::mergingOutput(int tab_start[], int tab_end[], int sizeOfTab, int tmp_tab[])
{
	int start_count = 0, end_count = 0;
	for (int i = 0; i<2 * sizeOfTab; i++)
	{
		if ((tab_start[start_count] <= tab_end[end_count] && start_count<sizeOfTab) || end_count == sizeOfTab)
		{
			tmp_tab[i] = tab_start[start_count];
			start_count++;
		}
		else
		{
			if (end_count<sizeOfTab)
			{
				tmp_tab[i] = tab_end[end_count];
				end_count++;
			}
		}
	}
}


ThreadedMergeSort::ThreadedMergeSort()
{
}


ThreadedMergeSort::~ThreadedMergeSort()
{
}
