﻿#include "SingleThreadMergeSort.h"
#include <cmath>


void SingleThreadMergeSort::mergingSingleThreadBasedOnMulti(int tab_start[], int tab_end[], int sizeOfTab)
{
	int * tmp_tab = new int[2 * sizeOfTab];
	int start_count = 0, end_count = 0;
	for (int i = 0; i<2 * sizeOfTab; i++)
	{
		if ((tab_start[start_count] <= tab_end[end_count] && start_count<sizeOfTab) || end_count == sizeOfTab)
		{
			tmp_tab[i] = tab_start[start_count];
			start_count++;
		}
		else
		{
			if (end_count<sizeOfTab)
			{
				tmp_tab[i] = tab_end[end_count];
				end_count++;
			}
		}
	}

	for (int i = 0; i<2 * sizeOfTab; i++)
	{
		tab_start[i] = tmp_tab[i];
	}
	delete[] tmp_tab;
}

void SingleThreadMergeSort::SingleThreadBasedOnMulti(int tab[], int size, int step)
{
	//step = 1 on start
	int t_count = ceil(size / (2 * step));
	if (t_count == 0)
	{
		return;
	}

	for (int i = 0; i < t_count; ++i) {

		int start = 2 * step * i;
		int end = 2 * step * i + step;

		//int end = 2 *  step * i + (step-1)*2+1; - tu jest koniec tablicy, a nie początek

		mergingSingleThreadBasedOnMulti(&tab[start], &tab[end], step);
	}

	if (2 ^ step<size)
	{
		SingleThreadBasedOnMulti(tab, size, step * 2);
	}
}

/*void SingleThreadMergeSort::merging(int tab[], int start, int center, int end)
{
	int *mergTab = new int[(end - start) + 1];
	int l = start; //sterowanie lew� cz�ci�
	int p = center + 1; //sterowanie praw� cz�ci�
	int m = 0; //sterowanie posortowan� tablic�(wpisywanie do mergTab)

	while (l <= center && p <= end)
	{
		if (tab[l] <= tab[p])
		{
			mergTab[m] = tab[l];
			m++;
			l++;
		}
		else
		{
			mergTab[m] = tab[p];
			m++;
			p++;
		}
	}

	if (l <= center || p <= end)
	{
		if (l <= center)
		{
			for (int i = 0; i <= (center - l); i++)
			{
				mergTab[m + i] = tab[l + i];
			}
		}
		else if (p <= end)
		{
			for (int i = 0; i <= (end - p); i++)
			{
				mergTab[m + i] = tab[p + i];
			}
		}
	}

	for (int i = 0; i <= (end - start); i++)
	{
		tab[start + i] = mergTab[i];
	}
}




void SingleThreadMergeSort::mergeSort(int tab[], int start, int end)
{
	if (start != end)
	{
		mergeSort(tab, start, (start + end) / 2);//podziały lewo
		mergeSort(tab, (start + end) / 2 + 1, end); //prawo
		merging(tab, start, (start + end) / 2, end);//scalanie
	}
}*/

SingleThreadMergeSort::SingleThreadMergeSort()
{
}


SingleThreadMergeSort::~SingleThreadMergeSort()
{
}
