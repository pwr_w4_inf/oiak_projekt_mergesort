#pragma once
class ThreadedMergeSort
{
public:

	static void mergingThread(int tab_start[], int tab_end[], int sizeOfTabs);
	static void NoThreadLimit(int tab[], int size, int step);

	static void ThreadLimit(int tab[], int size, int step);

	static void DivThread(int *tab, int size, int number);
	static void DivThreadV2(int *tab, int **divTab, int size, int number);

	static void mergingOutput(int tab_start[], int tab_end[], int sizeOfTab, int tmp_tab[]);

	ThreadedMergeSort();
	~ThreadedMergeSort();
};

