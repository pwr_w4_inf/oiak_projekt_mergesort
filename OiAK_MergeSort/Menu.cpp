﻿#include "Menu.h"
#include <iostream>
#include <windows.h>
#include "SingleThreadMergeSort.h"
#include "ThreadedMergeSort.h"

#define divTab 2 //podzia³ tablicy do div thread

void Menu::MenuView()
{
	double timeSingleThread = 0;
	double timeMultiThreadNoLimit = 0;
	double timeMultiThreadLimit = 0;
	double timeDivThread = 0;
	int powt = 1;
	int podzialy = 2;
	double timeDivThreadV2 = 0;


	std::cout << "Podaj ilosc powtorzen operacji: ";
	std::cin >> powt;
	std::cout << "Podaj ilosc podzialow tablicy: ";
	std::cin >> podzialy;
	std::cout << std::endl;



	for (int i = 0; i < powt; i++)
	{
		StartCounter();
		SingleThreadMergeSort::SingleThreadBasedOnMulti(tab, tabSize, 1);
		timeSingleThread += GetCounter();

		ValidateSortedTab();
		RestoreTabFromBackup();

		StartCounter();
		ThreadedMergeSort::DivThread(tab, tabSize, divTab);
		timeDivThread += GetCounter();

		ValidateSortedTab();
		RestoreTabFromBackup();

		//podział na 2 nowe tablice
		int * _divTab[2];

		_divTab[0] = new int[tabSize / 2];
		_divTab[1] = new int[tabSize / 2];

		memcpy(_divTab[0], tab, 4 * tabSize / 2);
		memcpy(_divTab[1], &tab[tabSize / 2], 4 * tabSize / 2);

		StartCounter();
		ThreadedMergeSort::DivThreadV2(tab,_divTab, tabSize, divTab);
		timeDivThreadV2 += GetCounter();
		delete[] _divTab[0];
		delete[] _divTab[1];

		ValidateSortedTab();
		RestoreTabFromBackup();

		StartCounter();
		ThreadedMergeSort::NoThreadLimit(tab, tabSize, 1);
		timeMultiThreadNoLimit += GetCounter();

		ValidateSortedTab();
		RestoreTabFromBackup();

		StartCounter();
		ThreadedMergeSort::ThreadLimit(tab, tabSize, 1);
		timeMultiThreadLimit += GetCounter();

		ValidateSortedTab();
		RestoreTabFromBackup();
	}

	timeSingleThread /= powt;
	timeMultiThreadNoLimit /= powt;
	timeMultiThreadLimit /= powt;
	timeDivThread /= powt;
	timeDivThreadV2 /= powt;

	std::cout << "Single thread sorting time "<< timeSingleThread <<std::endl;
	std::cout << "Multi thread no limit sorting time "<< timeMultiThreadNoLimit <<std::endl;
	std::cout << "Multi thread limit sorting time "<< timeMultiThreadLimit <<std::endl;
	std::cout << "Div thread sorting time "<< timeDivThread <<std::endl;
	std::cout << "Div threadv2 sorting time " << timeDivThreadV2 << std::endl;

}

void Menu::SingleMergeTest()
{
	StartCounter();
	SingleThreadMergeSort::SingleThreadBasedOnMulti(tab, tabSize, 1);
	std::cout<<"Single thread sorting time "<<GetCounter()<<std::endl;
}

void Menu::ThreadNoLimitMergeTest()
{
	StartCounter();
	ThreadedMergeSort::NoThreadLimit(tab, tabSize, 1);
	std::cout << "Multi thread sorting time " << GetCounter() << std::endl;
}

void Menu::ThreadLimitThreadMergeTest()
{
	StartCounter();
	ThreadedMergeSort::ThreadLimit(tab, tabSize, 1);
	std::cout << "Multi thread sorting time " << GetCounter() << std::endl;
}

void Menu::DivTrhreadTest()
{
	StartCounter();
	ThreadedMergeSort::DivThread(tab, tabSize, divTab);
	std::cout << "Dive thread sorting time " << GetCounter() << std::endl;
}

void Menu::MakeRandomNumbers()
{
	int *tab_tmp = new int[tabSize];

	for (int i = 0; i < tabSize; i++)
		tab_tmp[i] = std::rand()%(tabSize*10);

	if (tab_backup!=nullptr)
	{
		delete[] tab_backup;
	}

	tab_backup = tab_tmp;
	RestoreTabFromBackup();
}

void Menu::ValidateSortedTab()
{
	bool tabOK = true;
	for (int i = 0; i < tabSize-1; i++)
	{
		if (tab[i] > tab[i + 1])
		{
			std::cout << "Tablica zle posortowana"<<std::endl;
			tabOK = false;
			break;
		}
	}
	if (tabOK)
	{
		std::cout << "Tablica OK!"<<std::endl;
	}		
}

void Menu::RestoreTabFromBackup()
{
	if (tab!=nullptr)
	{
			delete[] tab;
	}

	tab = new int[tabSize];

	for (int i = 0; i < tabSize; i++)
	{
		tab[i] = tab_backup[i];
	}
}

void Menu::PrintTab(bool sorted)
{
	if(sorted)
	{
		std::cout << "Tablica posortowana: " << std::endl;
	}
	else
	{
		std::cout << "Tablica do posortowania: " << std::endl;
	}

	for (int i = 0; i < tabSize; i++)
		std::cout << tab[i] << " ";
	std::cout << std::endl;
}

void Menu::StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		std::cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}

double Menu::GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}

Menu::Menu(int _tabSize)
{
	tabSize = _tabSize;
	MakeRandomNumbers();
	MenuView();
}


Menu::~Menu()
{
}
