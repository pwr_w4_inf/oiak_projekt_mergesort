#pragma once
class Menu
{
private:
	int * tab = nullptr;
	int * tab_backup = nullptr;

	int tabSize = 0;


	double PCFreq = 0.0;
	__int64 CounterStart = 0;
public:

	void MenuView();

	void SingleMergeTest();
	void ThreadNoLimitMergeTest();
	void ThreadLimitThreadMergeTest();
	void DivTrhreadTest();

	void MakeRandomNumbers();
	void ValidateSortedTab();
	void RestoreTabFromBackup();
	void PrintTab(bool sorted);

	void StartCounter();
	double GetCounter();


	Menu(int tabSize);
	~Menu();
};

