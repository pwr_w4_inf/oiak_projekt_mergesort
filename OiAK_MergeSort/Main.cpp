#include <stdio.h>
#include <stdlib.h>
#include "Menu.h"
#include <ctime>
#include <iostream>


bool isPowerOf2(int a)
{
	if (0==a || 1==a)
	{
		return false;
	}

	if (a & a -1)
	{
		return false;
	}


	return true;
}


int main()
{

	int rozmiar = 1024;
	srand(time(NULL));

	do
	{
		std::cout << "Podaj rozmiar tablicy(tylko potegi dwojki): ";
		std::cin >> rozmiar;
	}
	while (!isPowerOf2(rozmiar));

	Menu * _menu =  new Menu(rozmiar);
	system("PAUSE");

	while (true)
	{
		int odp = 0;
		std::cout << "Czy chcesz powt�rzy�?\nTak-1\nNie-2\n";
		std::cin >> odp;
		if (odp == 1)
		{
			do
			{
				std::cout << "Podaj rozmiar tablicy(tylko potegi dwojki): ";
				std::cin >> rozmiar;
			} while (!isPowerOf2(rozmiar));
			delete _menu;
			_menu = new Menu(rozmiar);
			system("PAUSE");
		}
		else
		{
			system("PAUSE");
			return 0;
		}
	}
}